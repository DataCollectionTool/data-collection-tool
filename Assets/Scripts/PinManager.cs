﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PinManager : MonoBehaviour, IPointerClickHandler, IDragHandler, IEndDragHandler
{

    //Script Functionality - Manages clicking on the map and spawning a pin with the pin class and adds it to a list to hold all pins

    #region Variables

    //reference for pin to spawn 
    [SerializeField] private Image pinPrefab;

    //bool to decide between whether we're dragging or just clicking 
    bool isDragging;

    //list of all spawned pins, used for deleting or removing pins 
    public List<Pin> pins = new List<Pin>();

    //index of what pin we're on 
    int index = 0;

    #endregion

    //called when dragging so we dont confuse dragging with a click to select or place
    public void OnDrag(PointerEventData eventData)
    {
        isDragging = true;
    }

    //called when dragging so we dont confuse dragging with a click to select or place
    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
    }

    //function called when clicking on the map
    public void OnPointerClick(PointerEventData eventData)
    {
        //is dragging bool check 
        if (!isDragging && StateManager.pinModeState == StateManager.PinMode.Placing && Input.touchCount != 2)
        {
            //ups the index we're on 
            index++;

            //instantiates the pin as an image
            Image pin = Instantiate<Image>(pinPrefab, this.transform);

            //positions the pin where the cursor was
            pin.transform.position = Input.mousePosition;

            //assigns the Pin class to the pin 
            Pin pinData = pin.gameObject.AddComponent<Pin>();

            //initilases the data on the pin
            pinData.Data("Pin " + Random.Range(0,9999), pin.transform.position, PinUI.Instance.pinType.text, "", "", "", "", "");

            //adds the pin to the list of all pins
            pins.Add(pinData);

            PinUI.Instance.GetCurrentPin(pinData.pinName);

            PinUI.Instance.ShowPanel();

            PinUI.Instance.addpinToggle.isOn = false;
        }

    }

    public void RemovePin(Pin pin)
    {
        GameObject pinObj = null;

        foreach (Pin individualPin in pins)
        {
            if(individualPin == pin)
            {
                pinObj = individualPin.gameObject;
                pins.Remove(individualPin);
                break;
            }
        }
        Destroy(pinObj);
        PinUI.Instance.pinPanel.SetActive(false);
    }
}
