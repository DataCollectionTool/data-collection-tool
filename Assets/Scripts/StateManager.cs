﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class StateManager{

    public enum PinMode
    {
        Placing,
        Normal
    }

    public static PinMode pinModeState = PinMode.Normal;
}
