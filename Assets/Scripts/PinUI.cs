﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinUI : MonoBehaviour
{
    //script functionality that controls all the UI of the pins

    #region Variables

    //panel info for a pin when it is selected 
    public GameObject pinPanel;

    //dropdown that the user can choose options from 
    public Dropdown dropdown;

    public Text defectTypeDropdown;

    public Text assetTypeDropdown;

    public Text isThereDefectDropdown;

    public Toggle[] frameDefectToggles;

    public InputField inputNameField;
    public Text inputText;

    //singleton pattern for this script so each Pin can access it easily 
    public static PinUI Instance;

    //reference to the pin manager that holds all the references for the pins
    public PinManager pinManager;

    //current selected pin instance
    public Pin currentPin;

    //text field to change out the pin name 
    public Text pinName;

    public GameObject pinTypeOverlay;
    public Text pinType;

    public Toggle addpinToggle;

    public Scrollbar surveryScrollBar;
    #endregion

    //assign singleton instance and set panel to inactive 
    private void Awake()
    {
        Instance = this;
        pinPanel.SetActive(false);
    }

    //function that assigns the corresponding dropdown based on the pins dropdown string 
    public void AssignDropdown(string value)
    {
        int dropdownIndex = 0;

        for (int i = 0; i < dropdown.options.Count; i++)
        {
            if (dropdown.options[i].text == value)
            {
                dropdownIndex = i;
            }
        }
        dropdown.value = dropdownIndex;
    }

    //assigns the text to be the pins name
    public void AssignName()
    {
        pinName.text = currentPin.pinName;
    }

    //resets the dropdown when a new pin is spawned
    public void ResetDropdown()
    {
        int dropdownIndex = 0;
        dropdown.value = dropdownIndex;

        defectTypeDropdown.text = "";
        assetTypeDropdown.text = "";
        isThereDefectDropdown.text = "";

        for (int i = 0; i < frameDefectToggles.Length; i++)
        {
            frameDefectToggles[i].isOn = false;
        }
        //TODO currentPin.frameDefects = PinUI.Instance.frameDefectToggles.options[PinUI.Instance.isThereDefectDropdown.value].text;
        inputNameField.text = "";
        inputText.text = "";
    }

    //this is used to toggle the enum state incase you want to toggle in and out of being able to place pins
    public void TogglePinState(bool value)
    {
        if (value)
        {
            StateManager.pinModeState = StateManager.PinMode.Placing;
            pinTypeOverlay.SetActive(true);
        }
        else
        {
            StateManager.pinModeState = StateManager.PinMode.Normal;
        }
    }

    public void ShowPanel()
    {
        surveryScrollBar.value = 1;
        // sets the pin info panel to active
        pinPanel.SetActive(true);

        AssignName();

        //null or empty string check as the pin was initialised with this field empty, if its not empty grab its info
        ResetDropdown();

        GetSelectedInfo();
    }
    //closes the ui panel and sets current pin to inactive
    public void Close()
    {
        pinPanel.SetActive(false);
        SaveChanges();
        currentPin = null;
    }

    //function that calls corresponding UI updates
    public void GetSelectedInfo()
    {
        AssignDropdown(currentPin.pinType);
        AssignName();
        AssignData();
    }

    //function that saves the dropdown changes on the current selected pin 
    public void SaveChanges()
    {
        currentPin.defectType = defectTypeDropdown.text;
        currentPin.assetType = assetTypeDropdown.text;
        currentPin.isThereDefect = isThereDefectDropdown.text;

        for (int i = 0; i < frameDefectToggles.Length; i++)
        {
            if (PinUI.Instance.frameDefectToggles[i].isOn)
            {
                currentPin.frameDefects = frameDefectToggles[i].transform.GetComponentInChildren<Text>().text;
            }
        }
        currentPin.inputName = inputNameField.text;

        print(string.Format("{0},PinType:{1},Pin X:{2},PinY:{3}", currentPin.pinName, currentPin.pinType, currentPin.pinPos.x, currentPin.pinPos.y));
    }

    public void AssignData()
    {
        defectTypeDropdown.text = currentPin.defectType;
        assetTypeDropdown.text = currentPin.assetType;
        isThereDefectDropdown.text = currentPin.isThereDefect;

        for (int i = 0; i < frameDefectToggles.Length; i++)
        {
            if (frameDefectToggles[i].transform.GetComponentInChildren<Text>().text == currentPin.frameDefects)
            {
                frameDefectToggles[i].isOn = true;
            }
        }
        inputNameField.text = currentPin.inputName;
        inputText.text = currentPin.inputName;
    }

    //function that grabs a reference to the currently selected pin, has to loop through all potential pins 
    public void GetCurrentPin(string name)
    {
        //loops through all pins 
        foreach (Pin pin in pinManager.pins)
        {
            //if the pin matches the pin we're looking for assign it and break out of loop as pin has already been found 
            if (pin.pinName == name)
            {
                currentPin = pin;
                break;
            }
            else
            {
                //reset pin if no corresponding pin was found 
                currentPin = null;
            }
        }
    }

    public void DeleteButton()
    {
        pinManager.RemovePin(currentPin);
        currentPin = null;
    }
}
