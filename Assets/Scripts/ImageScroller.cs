﻿using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class ImageScroller : MonoBehaviour, IScrollHandler {

    private Vector3 startScale;

    [SerializeField] private float zoomSpeed = 0.1f;
    [SerializeField] private float maxZoom = 10f;

    private void Awake()
    {
        startScale = transform.localScale;
    }
    public void OnScroll(PointerEventData eventData)
    {
        var delta = Vector3.one * (eventData.scrollDelta.y * zoomSpeed);
        var desiredScale = transform.localScale + delta;

        desiredScale = ClampDesiredScale(desiredScale);
        transform.localScale = desiredScale;
    }

    private Vector3 ClampDesiredScale(Vector3 scale)
    {
        scale = Vector3.Max(startScale, scale);
        scale = Vector3.Min(startScale * maxZoom, scale);
        return scale;
    }
}
